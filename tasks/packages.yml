---
- name: Create packages.d directory
  file:
    path: "{{ packages_d }}"
    state: directory
  tags:
    - packages

- name: Write packages.d file
  template:
    src: packages_d_file.j2
    dest: "{{ packages_d_file }}"
    mode: 0644
  tags:
    - packages

- name: Collect packages.d files
  find:
    paths:
      - "{{ packages_d }}"
  register: packages_d_find
  tags:
    - packages

- name: Read packages.d files
  slurp:
    src: "{{ item }}"
  loop: "{{ packages_d_find.files | map(attribute='path') | list }}"
  register: packages_d_slurp
  tags:
    - packages

- name: Concatenate packages.d files
  set_fact:
    packages_d_list: "{{ packages_d_slurp.results | map(attribute='content') | map('b64decode') | join('\n') | from_yaml }}"
  tags:
    - packages

- name: Install world file
  template:
    src: world.j2
    dest: /var/lib/portage/world
    mode: 0644
  register: packages_world_result
  tags:
    - packages

- name: Check for package.accept_keywords path
  stat:
    path: /etc/portage/package.accept_keywords
    get_checksum: False
  register: package_accept_keywords_stat
  tags:
    - packages

- name: Remove package.accept_keywords dir
  file:
    path: /etc/portage/package.accept_keywords
    state: absent
  when: package_accept_keywords_stat.stat.exists and (package_accept_keywords_stat.stat.isdir or package_accept_keywords_stat.stat.islnk)
  tags:
    - packages

- name: Install package.accept_keywords
  template:
    src: package.accept_keywords.j2
    dest: /etc/portage/package.accept_keywords
    mode: 0644
  register: packages_accept_keywords_result
  tags:
    - packages

- name: Check for package.mask path
  stat:
    path: /etc/portage/package.mask
    get_checksum: False
  register: package_mask_stat
  tags:
    - packages

- name: Remove package.mask dir
  file:
    path: /etc/portage/package.mask
    state: absent
  when: package_mask_stat.stat.exists and (package_mask_stat.stat.isdir or package_mask_stat.stat.islnk)
  tags:
    - packages

- name: Install package.mask
  template:
    src: package.mask.j2
    dest: /etc/portage/package.mask
    mode: 0644
  register: package_mask_result
  tags:
    - packages

- name: Check for package.use path
  stat:
    path: /etc/portage/package.use
    get_checksum: False
  register: package_use_stat
  tags:
    - packages

- name: Remove package.use dir
  file:
    path: /etc/portage/package.use
    state: absent
  when: package_use_stat.stat.exists and (package_use_stat.stat.isdir or package_use_stat.stat.islnk)
  tags:
    - packages

- name: Install package.use
  template:
    src: package.use.j2
    dest: /etc/portage/package.use
    mode: 0644
  register: packages_use_result
  tags:
    - packages

- name: Check for package.license path
  stat:
    path: /etc/portage/package.license
    get_checksum: False
  register: package_license_stat
  tags:
    - packages

- name: Remove package.license dir
  file:
    path: /etc/portage/package.license
    state: absent
  when: package_license_stat.stat.exists and (package_license_stat.stat.isdir or package_license_stat.stat.islnk)
  tags:
    - packages

- name: Install package.license
  template:
    src: package.license.j2
    dest: /etc/portage/package.license
    mode: 0644
  register: packages_license_result
  tags:
    - packages

- name: Crate patches dir
  file:
    path: "/etc/portage/patches/{{ item.name }}"
    state: directory
  loop: "{{ packages }}"
  when: item.patch is defined

- name: Install patches
  template:
    src: "{{ item.1 }}"
    dest: "/etc/portage/patches/{{ item.0.name }}/{{ item.1 | splitext | first }}"
    mode: 0644
  register: packages_patches_result
  loop: "{{ packages | subelements('patch', true) }}"

- name: Check if configuration changed
  set_fact:
    packages_configuration_changed: "{{ packages_world_result.changed or packages_accept_keywords_result.changed or package_mask_result.changed or packages_use_result.changed or packages_license_result.changed or packages_patches_result.changed | bool }}"
  when: not ansible_check_mode
